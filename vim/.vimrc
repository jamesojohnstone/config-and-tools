set completeopt-=preview

"let mapleader=" " "replace ctrl with space for mappings
map <space> <leader>

inoremap jj <ESC> "bind jj to esc, leave 1 sec between to type 2 j's

set backupdir=./.backup,.,/tmp
set directory=.,./.backup,/tmp

set hlsearch "turn on search highlighting

au BufNewFile,BufRead *.py
    \ 2match Overlength /\%80v.\+/ "mark lines above 79 length

set nu "turn on line numbering

au BufNewFile,BufRead *.js, *.html, *.css
    \ set tabstop=2 |
    \ set softtabstop=2 |
    \ set shiftwidth=2

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'ntpeters/vim-better-whitespace'
Plugin 'lifepillar/vim-solarized8'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'tpope/vim-fugitive'
call vundle#end()

filetype on
filetype indent on
filetype plugin on

let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR> "press G to go to definition

let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_max_files = 0
let g:ctrlp_max_depth = 40
let g:ctrlp_extensions = ['buffertag', 'tag', 'line', 'dir']


syntax enable
" set directory=~/.vim/swapfiles//
set backupdir=~/.vim/swapfiles//
set background=dark
colorscheme solarized8
set t_Co=256
set t_ut=
"highlight lines above 79 chars
highlight Overlength ctermbg=red ctermfg=white guibg=#592929
highlight ExtraWhitespace ctermbg=LightGreen

set noswapfile
set pastetoggle=<F3>
map <F4> :%s/\s\+$//e
